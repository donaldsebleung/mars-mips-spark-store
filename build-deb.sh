#!/bin/bash -e

mkdir -p ./dl-assets/
wget -O ./dl-assets/Mars4_5.jar https://courses.missouristate.edu/KenVollmar/mars/MARS_4_5_Aug2014/Mars4_5.jar
dpkg-buildpackage -us -uc
lintian ../mars-mips_4.5~spark3_all.deb
