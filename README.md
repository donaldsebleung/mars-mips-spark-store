# mars-mips-spark-store

## 介绍

MARS MIPS 模拟器 deb 包，供[星火应用商店](https://www.spark-app.store/)上架

## 使用说明

于仓库根目录执行以下命令：

```bash
$ ./build-deb.sh
```

## 许可证

[MIT](./LICENSE)
